import random
import heapq
import copy
from math import sqrt

import controller_template as controller_template


class Controller(controller_template.Controller):

    def __init__(self, track, evaluate=True):
        super().__init__(track, evaluate=evaluate)

    def take_action(self, parameters: list) -> int:
        """
        :param parameters: Current weights/parameters of your controller

        :return: An integer corresponding to an action:
        1 - UP
        2 - NOTHING (GRAVITY)

        """
        features = self.compute_features(self.sensors)

        up = parameters[0] + parameters[1]*features[1] + parameters[2]*features[2] + parameters[3]*features[3] + parameters[4]*features[4] + parameters[5]*features[5]
        down = parameters[6] + parameters[7]*features[1] + parameters[8]*features[2] + parameters[9]*features[3] + parameters[10]*features[4] + parameters[11]*features[5]

        if up >= down:
            return 1
        else:
            return 2

    def compute_features(self, sensors):
        """
        :sensors: List that contains (in order) the the following sensors (ranges of each sensor are also provided below)

        0 - water_UP: 1-700
        1 - water_UP_RIGHT: 1-700
        2 - obstacle_UP: 1-700
        3 - obstacle_UP_RIGHT: 1-700
        4 - obstacle_AHEAD: 1-700
        5 - obstacle_DOWN_RIGHT: 1-700
        6 - obstacle_DOWN: 1-700
        7 - monster_UP: 1-200
        8 - monster_UP_RIGHT: 1-200
        9 - monster_AHEAD: 1-200
        10 - monster_DOWN_RIGHT: 1-200
        11 - monster_DOWN: 1-200
        12 - oxygen: 1-400

        """
        # Considering the dimensions of the submarine bounding box
        water_max = 700
        water_min = 10
        ahead_max = 200
        ahead_min = 25
        upright_max = 200
        upright_min = sqrt(200)
        downright_max = 200
        downright_min = sqrt(450)

        # Centrality feature (-1, 1)
        down = min(sensors[6], sensors[11])
        up = min(sensors[2], sensors[7])
        centrality = (up - down)/(up + down)

        # Water feature (0, 1)
        water = 0
        if sensors[0] != 700:
            water = 1

        #water45 = -sensors[1] + 700 + upright_min
        #water45 = normalize01(water45, lambda x: x, upright_min, upright_max)

        # Ahead, Upright and Downright features (0, 1)
        ahead = min(sensors[9], sensors[4])
        ahead = -ahead + ahead_min + ahead_max
        ahead = normalize01(ahead, lambda x: x**2, ahead_min, ahead_max)

        upright = min(sensors[8], sensors[3])
        upright = -upright + upright_min + upright_max
        upright = normalize01(upright, lambda x: x**2, upright_min, upright_max)

        downright = min(sensors[5], sensors[10])
        downright = -downright + downright_min + downright_max
        downright = normalize01(downright, lambda x: x**2, downright_min, downright_max)

        return [1, water, centrality, ahead, upright, downright]

    def learn(self, weights) -> list:
        """
        Genetic Algorithm

        :param weights: initial weights of the controller (either loaded from a file or generated randomly)
        :return: the best weights found by your learning algorithm, after the learning process is over
        """

        genome = Genome(size=len(weights), min_allele=-10, max_allele=10, alleles=weights)
        ga = GeneticAlgorithm(genome, pop_size=50, num_generations=50, mutation_rate=0.1, elitism_rate=0.05, write_result=True)
        ga.fitness_function = self.run_episode
        ga.evolve()

        return [g.alleles for g in ga.best_genomes]


class GeneticAlgorithm:

    def __init__(self, genome, pop_size=50, num_generations=100, mutation_rate=0.01, elitism_rate=0.1, write_result=False):

        self.pop_size = pop_size
        self.population = [genome] + [genome.random_clone() for _ in range(self.pop_size - 1)]
        self.num_generations = num_generations
        self.termination_criteria = gen_limit_termination_criteria
        self.crossover = uniform_crossover
        self.selection = tournament_selection
        self.fitness_function = None
        self.mutation_rate = mutation_rate
        self.elitism_rate = elitism_rate
        self.best_genomes = [] # Top 5 genomes
        self.generation = 1
        self.write_result = write_result
        if write_result:
            self.file = open("ga_p{}m{}e{}.txt".format(self.pop_size, self.mutation_rate, self.elitism_rate), 'w+')

    def evolve(self):

        # First generation
        self.eval(self.population)
        self.best_genomes = heapq.nlargest(5, self.population, lambda x: x.score)
        print("Generation {}".format(self.generation))
        for g in self.best_genomes:
            print(g)
        if self.write_result:
            self.file.write("{} {}\n".format(self.generation, self.best_genomes[0].score))
        self.generation += 1

        while not self.termination_criteria(self):
            next_pop = []

            # Selection and Reprodution
            num_new_child = int((1 - self.elitism_rate) * self.pop_size)
            while len(next_pop) < num_new_child:
                father = self.selection(self.population)
                mother = self.selection([g for g in self.population if g is not father])
                child1, child2 = self.crossover(father, mother)
                next_pop.extend([child1, child2])

            # Mutation
            for g in next_pop:
                g.mutate(self.mutation_rate)

            # Evaluation
            self.eval(next_pop)
            self.best_genomes = heapq.nlargest(5, self.best_genomes + next_pop, key=lambda x: x.score)

            # Elitism
            next_pop.extend(heapq.nlargest(self.pop_size - len(next_pop), self.population, key=lambda x: x.score))

            # Update population
            self.population = next_pop

            print("Generation {}".format(self.generation))
            for g in self.best_genomes:
                print(g)
            if self.write_result:
                self.file.write("{} {}\n".format(self.generation, self.best_genomes[0].score))

            self.generation += 1

        if self.write_result:
            self.file.close()

    def eval(self, genomes):
        for g in genomes:
            g.score = self.fitness_function(g.alleles)


class Genome:

    def __init__(self, size, min_allele, max_allele, alleles=None):

        self.min_allele = min_allele
        self.max_allele = max_allele
        self.score = None
        self.size = size
        if alleles is None:
            self.alleles = [random.uniform(self.min_allele, self.max_allele) for _ in range(size)]
        else:
            self.alleles = alleles

    def mutate(self, mutation_rate):
        for i in range(self.size):
            if random.random() < mutation_rate:
                self.alleles[i] = random.uniform(self.min_allele, self.max_allele)

    def random_clone(self):
        genome_clone = copy.deepcopy(self)
        for i in range(self.size):
            genome_clone.alleles[i] = random.uniform(self.min_allele, self.max_allele)

        return genome_clone

    def __str__(self):

        return 'Genome=(' + ' '.join(["{0:0.2f}".format(i) for i in self.alleles]) + ') Score=' + str(self.score)


def single_point_crossover(genome1, genome2):
    child1 = copy.deepcopy(genome1)
    child2 = copy.deepcopy(genome2)

    cut_index = random.randint(1, genome1.size - 2)
    child1.alleles[:cut_index], child1.alleles[cut_index:] = genome1.alleles[:cut_index], genome2.alleles[cut_index:]
    child2.alleles[:cut_index], child2.alleles[cut_index:] = genome2.alleles[:cut_index], genome1.alleles[cut_index:]

    return child1, child2


def uniform_crossover(genome1, genome2):
    child1 = copy.deepcopy(genome1)
    child2 = copy.deepcopy(genome2)
    mask = [random.random() < 0.5 for _ in range(genome1.size)]
    for index, n in enumerate(mask):
        if n:
            child1.alleles[index], child2.alleles[index] = child2.alleles[index], child1.alleles[index]

    return child1, child2


def gen_limit_termination_criteria(ga):
    if ga.generation > ga.num_generations:
        return True
    else:
        return False


def tournament_selection(pop):
    '''
    Tournament Selection
    Return the best genome from a sample of the population of size len(pop)*0.1
    :param pop: The population from which the genome will be selected
    :return: The genome selected
    '''
    return max(random.sample(pop, int(len(pop)*0.1)), key=lambda g: g.score)


def spin_wheel_selection(pop):
    s = sum(g.score for g in pop)
    r = random.uniform(0, s)
    t = 0
    for g in pop:
        t += g.score
        if t >= r:
            return g

def normalize01(x, func, a, b):
    a, b = min(func(a), func(b)), max(func(a), func(b))

    return (func(x) - a) / (b - a)

