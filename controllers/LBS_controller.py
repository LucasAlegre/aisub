import random
import heapq
import copy
from math import sqrt

import controller_template as controller_template


def normalize01(x, func, a, b):
    a, b = min(func(a), func(b)), max(func(a), func(b))

    return (func(x) - a) / (b - a)

class Controller(controller_template.Controller):

    def __init__(self, track, evaluate=True):
        super().__init__(track, evaluate=evaluate)
        self.old_sensors = [0 for _ in range(14)]

    def take_action(self, parameters: list) -> int:
        """
        :param parameters: Current weights/parameters of your controller

        :return: An integer corresponding to an action:
        1 - UP
        2 - NOTHING (GRAVITY)

        """
        features = self.compute_features(self.sensors)

        up = parameters[0] + parameters[1]*features[1] + parameters[2]*features[2] + parameters[3]*features[3] + parameters[4]*features[4] + parameters[5]*features[5]
        down = parameters[6] + parameters[7]*features[1] + parameters[8]*features[2] + parameters[9]*features[3] + parameters[10]*features[4] + parameters[11]*features[5]

        if up >= down:
            return 1
        else:
            return 2

    def compute_features(self, sensors):
        """
        :sensors: List that contains (in order) the the following sensors (ranges of each sensor are also provided below)

        0 - water_UP: 1-700
        1 - water_UP_RIGHT: 1-700
        2 - obstacle_UP: 1-700
        3 - obstacle_UP_RIGHT: 1-700
        4 - obstacle_AHEAD: 1-700
        5 - obstacle_DOWN_RIGHT: 1-700
        6 - obstacle_DOWN: 1-700
        7 - monster_UP: 1-200
        8 - monster_UP_RIGHT: 1-200
        9 - monster_AHEAD: 1-200
        10 - monster_DOWN_RIGHT: 1-200
        11 - monster_DOWN: 1-200
        12 - oxygen: 1-400

        """
        # Considering the dimensions of the submarine bounding box
        water_max = 700
        water_min = 10
        ahead_max = 200
        ahead_min = 25
        upright_max = 200
        upright_min = sqrt(200)
        downright_max = 200
        downright_min = sqrt(450)

        # Centrality feature (-1, 1)
        down = min(sensors[6], sensors[11])
        up = min(sensors[2], sensors[7])
        centrality = (up - down)/(up + down)

        # Water feature (0, 1)
        water = 0
        if sensors[0] != 700:
            water = 1

        #water45 = -sensors[1] + 700 + upright_min
        #water45 = normalize01(water45, lambda x: x, upright_min, upright_max)

        # Ahead, Upright and Downright features (0, 1)
        ahead = min(sensors[9], sensors[4])
        ahead = -ahead + ahead_min + ahead_max
        ahead = normalize01(ahead, lambda x: x**2, ahead_min, ahead_max)

        upright = min(sensors[8], sensors[3])
        upright = -upright + upright_min + upright_max
        upright = normalize01(upright, lambda x: x**2, upright_min, upright_max)

        downright = min(sensors[5], sensors[10])
        downright = -downright + downright_min + downright_max
        downright = normalize01(downright, lambda x: x**2, downright_min, downright_max)

        self.old_sensors = sensors

        return [1, water, centrality, ahead, upright, downright]

    def learn(self, weights) -> list:
        """
        IMPLEMENT YOUR LEARNING METHOD (i.e. YOUR LOCAL SEARCH ALGORITHM) HERE

        HINT: you can call self.run_episode (see controller_template.py) to evaluate a given set of weights
        :param weights: initial weights of the controller (either loaded from a file or generated randomly)
        :return: the best weights found by your learning algorithm, after the learning process is over
        """
        
        lbs = LocalBeamSearch(size=len(weights), num_candidates=5, num_neighbors=50, num_iterations=100, max_changes=8)
        lbs.fitness_function = self.run_episode
        lbs.run()

        return [x.weights for x in lbs.current_candidates]


class LocalBeamSearch:

    def __init__(self, size, num_candidates, num_neighbors, num_iterations, max_changes, candidates=None):
        self.size = size
        self.min_value = -10
        self.max_value = 10
        self.num_candidates = num_candidates
        self.current_candidates = []
        self.candidates = candidates
        self.num_neighbors = num_neighbors
        self.inc_neighbor = 2
        self.neighbors = []
        self.fitness_function = None
        self.num_iterations = num_iterations
        self.iteration = 1
        self.max_changes = max_changes
        
    def random_candidates(self):
        for _ in range(self.num_candidates):
            weights = [random.uniform(self.min_value, self.max_value) for _ in range(self.size)]
            score = self.fitness_function(weights)
            candidate = Candidate(weights, score)
            self.current_candidates += [candidate]

    def get_neighbors(self, current):
        neighbors = []
        for _ in range(self.num_neighbors):
            weights = current.weights.copy()
            changes = random.randint(1, self.max_changes)
            for i in random.sample([x for x in range(0, self.size)], changes):
                
                inc =  random.uniform(-self.inc_neighbor, self.inc_neighbor)                
                weights[i] += inc
            
            score = self.fitness_function(weights)
            neighbor = Candidate(weights, score)
            neighbors.append(neighbor)

        return neighbors

    def run(self):
        if self.candidates is None:
            self.random_candidates()
        else:
            for weights in self.candidates:
                candidate = Candidate(weights, self.fitness_function(weights))
                self.current_candidates.append(candidate)

        print("Iteration 0:")
        for candidate in self.current_candidates:
            print(candidate)

        while self.iteration <= self.num_iterations:
            
            #neighbors
            self.neighbors = []
            for candidate in self.current_candidates:
                self.neighbors.append(candidate)
                self.neighbors.extend(self.get_neighbors(candidate))

            #best candidates
            self.current_candidates = heapq.nlargest(self.num_candidates, self.neighbors, lambda x: x.score)

            print("Iteration "+str(self.iteration)+":")
            for candidate in self.current_candidates:
                print(candidate)

            self.iteration += 1

class Candidate:
    def __init__(self, weights, score):
        self.weights = weights
        self.score = score

    def __str__(self):
        return 'state = ' + str(["{0:0.2f}".format(i) for i in self.weights]) + ' score = ' + str(self.score);
