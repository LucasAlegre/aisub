import random
import heapq
import copy
import math
from math import sqrt

import controller_template as controller_template


def normalize01(x, func, a, b):
    a, b = min(func(a), func(b)), max(func(a), func(b))

    return (func(x) - a) / (b - a)

class Controller(controller_template.Controller):

    def __init__(self, track, evaluate=True):
        super().__init__(track, evaluate=evaluate)
        self.old_sensors = [0 for _ in range(14)]

    def take_action(self, parameters: list) -> int:
        """
        :param parameters: Current weights/parameters of your controller

        :return: An integer corresponding to an action:
        1 - UP
        2 - NOTHING (GRAVITY)

        """
        features = self.compute_features(self.sensors)

        up = parameters[0] + parameters[1]*features[1] + parameters[2]*features[2] + parameters[3]*features[3] + parameters[4]*features[4] + parameters[5]*features[5]
        down = parameters[6] + parameters[7]*features[1] + parameters[8]*features[2] + parameters[9]*features[3] + parameters[10]*features[4] + parameters[11]*features[5]

        if up >= down:
            return 1
        else:
            return 2

    def compute_features(self, sensors):
        """
        :sensors: List that contains (in order) the the following sensors (ranges of each sensor are also provided below)

        0 - water_UP: 1-700
        1 - water_UP_RIGHT: 1-700
        2 - obstacle_UP: 1-700
        3 - obstacle_UP_RIGHT: 1-700
        4 - obstacle_AHEAD: 1-700
        5 - obstacle_DOWN_RIGHT: 1-700
        6 - obstacle_DOWN: 1-700
        7 - monster_UP: 1-200
        8 - monster_UP_RIGHT: 1-200
        9 - monster_AHEAD: 1-200
        10 - monster_DOWN_RIGHT: 1-200
        11 - monster_DOWN: 1-200
        12 - oxygen: 1-400

        """
        # Considering the dimensions of the submarine bounding box
        water_max = 700
        water_min = 10
        ahead_max = 200
        ahead_min = 25
        upright_max = 200
        upright_min = sqrt(200)
        downright_max = 200
        downright_min = sqrt(450)

        # Centrality feature (-1, 1)
        down = min(sensors[6], sensors[11])
        up = min(sensors[2], sensors[7])
        centrality = (up - down)/(up + down)

        # Water feature (0, 1)
        water = 0
        if sensors[0] != 700:
            water = 1

        #water45 = -sensors[1] + 700 + upright_min
        #water45 = normalize01(water45, lambda x: x, upright_min, upright_max)

        # Ahead, Upright and Downright features (0, 1)
        ahead = min(sensors[9], sensors[4])
        ahead = -ahead + ahead_min + ahead_max
        ahead = normalize01(ahead, lambda x: x**2, ahead_min, ahead_max)

        upright = min(sensors[8], sensors[3])
        upright = -upright + upright_min + upright_max
        upright = normalize01(upright, lambda x: x**2, upright_min, upright_max)

        downright = min(sensors[5], sensors[10])
        downright = -downright + downright_min + downright_max
        downright = normalize01(downright, lambda x: x**2, downright_min, downright_max)

        self.old_sensors = sensors

        return [1, water, centrality, ahead, upright, downright]

    def learn(self, weights) -> list:
        """
        IMPLEMENT YOUR LEARNING METHOD (i.e. YOUR LOCAL SEARCH ALGORITHM) HERE

        HINT: you can call self.run_episode (see controller_template.py) to evaluate a given set of weights
        :param weights: initial weights of the controller (either loaded from a file or generated randomly)
        :return: the best weights found by your learning algorithm, after the learning process is over
        """

        sa = SimulatedAnnealing(size=len(weights), temperature=100.0, dec_temperature=0.92, num_iterations=50, max_changes=8)
        sa.fitness_function = self.run_episode
        sa.run()

        return sa.current_state


class SimulatedAnnealing:

    def __init__(self, size, temperature, dec_temperature, num_iterations, max_changes=8, initial_state=None):
        self.current_state = []
        self.size = size
        self.min_value = -10
        self.max_value = 10
        self.temperature = temperature
        self.dec_temperature = dec_temperature
        self.num_iterations = num_iterations
        self.max_changes = max_changes
        self.inc_neighbor = 2
        self.initial_state = initial_state
        self.fitness_function = None
        self.score = 0;


    def random_state(self):
        self.current_state = [random.uniform(self.min_value, self.max_value) for _ in range(self.size)]

    def update_temperature(self):
        self.temperature *= self.dec_temperature

    def get_random_neighbor(self, current):
        neighbor = current

        changes = random.randint(1, self.max_changes)
        for i in random.sample([x for x in range(0, self.size)], changes):
            inc =  random.uniform(-self.inc_neighbor, self.inc_neighbor)
            if inc > 0:
                inc = min(self.max_value, inc)
            else:
                inc = max(self.min_value, inc)
            neighbor[i] += inc

        return neighbor


    def run(self):
        if self.initial_state is None:
            self.random_state()
        else:
            self.current_state = self.initial_state

        while self.temperature > 0.01:
            self.update_temperature()
            for i in range(self.num_iterations):

                candidate = self.get_random_neighbor(self.current_state.copy())

                candidate_score = self.fitness_function(candidate)
                current_state_score = self.fitness_function(self.current_state)

                delta = candidate_score - current_state_score
                self.score = current_state_score

                if(delta > 0):
                    self.current_state = candidate
                    self.score = candidate_score
                else:
                    p = math.exp(delta/self.temperature)
                    if(random.random() < p):
                        self.current_state = candidate
                        self.score = candidate_score

            print("Temperature "+str("{0:0.2f}".format(self.temperature))+":")
            print(self)

    def __str__(self):
        return 'state = ' + str(["{0:0.2f}".format(i) for i in self.current_state]) + ' score = ' + str(self.score);
