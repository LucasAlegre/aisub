import matplotlib.pyplot as plt
from math import sqrt, log, log2, log10
from mpl_toolkits.mplot3d import Axes3D
import numpy as np


def normalize01(x, func, a, b):
    a, b = min(func(a), func(b)), max(func(a), func(b))

    return (func(x) - a) / (b - a)


if __name__ == '__main__':

    water_max = 700
    water_min = 10
    ahead_max = 200
    ahead_min = 25
    upright_max = 200
    upright_min = sqrt(200)
    downright_max = 200
    downright_min = sqrt(450)

    fig = plt.figure(1)

    x = [x for x in range(int(upright_min), water_max+1)]
    # water 45
    plt.subplot(221)
    plt.plot(x, [normalize01(i, lambda z: (-z + upright_min + water_max)**(1/4), upright_min, water_max) for i in x])
    plt.title("Water45 Feature")

    # Ahead
    x = [x for x in range(ahead_min, ahead_max+1)]
    plt.subplot(223)
    plt.plot(x, [normalize01(-i + ahead_max + ahead_min, lambda z: z**2, ahead_max, ahead_min) for i in x])
    plt.title("Ahead Feature")
    plt.xlabel("min(obstacle_AHEAD, monster_AHEAD)")
    plt.grid()

    # Upright Feature
    x = [x for x in range(int(upright_min), upright_max+1)]
    plt.subplot(224)
    plt.plot(x, [(i**(1/2) - upright_min**(1/2)) / (upright_max**(1/2) - upright_min**(1/2)) for i in x])
    plt.title("Upright Feature")
    plt.grid()

    '''
    # Centrality Feature
    def centrality(a, b):
        return (a-b)/(a+b)

    fig = plt.figure(2)
    ax = fig.add_subplot(111, projection='3d')
    x = [x for x in range(1,201)]
    X, Y = np.meshgrid(x,x)
    z = np.array([centrality(x, y) for x, y in zip(X, Y)])
    ax.plot_surface(X, Y, z)
    '''

    plt.show()
