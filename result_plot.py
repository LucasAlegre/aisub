import matplotlib.pyplot as plt
import sys


if __name__ == '__main__':

    plt.figure(1)
    for i in range(1, len(sys.argv)):
        with open(sys.argv[i], 'r') as f:
            l = f.readlines()
        x = []
        y = []
        label = l[0]
        for line in l[1:]:
            g, s = [int(n) for n in line.split()]
            x.append(g)
            y.append(s)
        x = x + [i for i in range(len(x)+1, 51)]
        y = y + [y[-1] for i in range(50 - len(y))]
        plt.plot(x, y, label=label)
    plt.title("Tamanho da População")
    plt.xlabel("Generation")
    plt.ylabel("Best Score")
    plt.xlim(xmin=1)
    plt.legend()
    plt.grid()
    plt.show()

